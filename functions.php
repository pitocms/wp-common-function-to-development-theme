<?php 

// Retrieve current theme directory.
get_template_directory(); 
// output example :c:\xampp\hidocs\wordpres\wp-content\themes\mytheme

// Retrieve theme directory URI. 
get_template_directory_uri(); 
// output example :http://localhost/wordpres/wp-content/themes/mytheme

//A safe way to register a CSS style file for later use with wp_enqueue_style(). 
wp_register_style( $handle, $src, $deps, $ver, $media );

// $handle = Name of the stylesheet (which should be unique as it is used to identify the script in the whole system). 

//tp register jquery 
wp_register_script( string $handle, string $src, array $deps = array(), string|bool|null $ver = false, bool $in_footer = false ) 

/* 
$in_footer
(bool) (Optional) Whether to enqueue the script before </body> instead of in the <head>. Default 'false'.
 Default value: false
 */

// enqueue css 
wp_enqueue_style( $handle, $src, $deps, $ver, $media );

//enqueue js 
wp_enqueue_script( $handle, $src, $deps, $ver, $in_footer );


#Handles_and_Their_Script_Paths_Registered_by_WordPress

// https://codex.wordpress.org/pt-br:Function_Reference/wp_register_script#Handles_and_Their_Script_Paths_Registered_by_WordPress